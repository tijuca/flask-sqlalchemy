from setuptools import setup

# Metadata goes in pyproject.toml. These are here for GitHub's dependency graph.
setup(name="Flask-SQLAlchemy", install_requires=["Flask>=2.0", "SQLAlchemy>=1.4"])
